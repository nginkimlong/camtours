from django.conf.urls import include, url
from django.urls import path
from employee import views as employee_view
from employee.views import *
from django.conf import settings 
from django.conf.urls.static import static

from employee.views import *


app_name = 'employee'
urlpatterns = [
    # url('^employee/detail', employee_view.detail),
    # url('^employee/', employee_view.index),

    # url('^employee', employee_view.index),
    # url('^employee/detail', employee_view.detail),

    # url('^$', employee_view.index, name='homepage'),
    url('^$', IndexView.as_view(), name='homepage'),
    url('^(?P<id>\d+)/(?P<name>\D+)/$', employee_view.show_employee_detail, name="employee_detail"),
    # url('^detail/$', employee_view.detail, name='detail'),
    url('^detail/$', detail.as_view(), name='detail'),
]