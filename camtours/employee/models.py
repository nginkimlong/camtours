from statistics import mode
from django.db import models

GENDER = (
    ('MALE', 'M'),
    ('FEMALE', 'F'),
)
class Employee(models.Model): 
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=120)
    sex = models.CharField(max_length=7, choices=GENDER)
    dob = models.DateTimeField(auto_created=False)
    email = models.EmailField(max_length=150)
    tel = models.CharField(max_length=20)
    address = models.TextField()
    status = models.BooleanField()
    def __str__(self):
        return str(self.first_name) + " " + str(self.last_name) 

