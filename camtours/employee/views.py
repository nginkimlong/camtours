from django.shortcuts import render
from employee.models import Employee
from django.views.generic import View, TemplateView 
from django.views import generic
from django.core.paginator import Paginator


# Create your views here.
# def index(request): 
#     # data = ["Student A", "Student B", "Student C", "Student D", "Student E"]
#     data = Employee.objects.filter() # SELECT * FROM Employee
#     return render(request, 'main/index.html', {'data': data})


class IndexView(generic.ListView): 
    template_name =  'main/index.html'
    context_object_name = 'data'
    paginate_by = 2
    def get_queryset(self):
        return Employee.objects.filter().order_by('-id')

    

# def detail(request): 
#     return render(request, 'employee/index.html')
# class detail(TemplateView):
#     template_name = 'employee/index.html'
#     def get_context_data(self, **kwargs):
#         return super(detail, self).get_context_data(**kwargs)
#         context['data']: 'Hello world'
#         return context

class detail(View): 
    template_name = 'employee/index.html'

    def dispatch(self, request, *args, **kwargs):
        print("Loading dispatching")
        return super().dispatch(request, *args, **kwargs)
   
    def get(self, request): 
        print("Hello world")
        data = Employee.objects.filter()
        print(data)
        return render(request, self.template_name, {'data': 'test'})
    
    def post(self, request):
        print("POST ME FROM FORM")
        return render(request, self.template_name, {'data': 'test'})


def show_employee_detail(request, id, name): 
    id = id
    # var1 = request.GET['var1']
    # print("Extra parameter is: ", var1)
    data = Employee.objects.filter(id=id) # SELECT * FROM Employee WHERE id = 1
    return render(request, 'employee/show_employee_detail.html', {'data': data})