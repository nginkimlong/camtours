from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from employee import views as employee_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('employee/', include('employee.urls')),

]
